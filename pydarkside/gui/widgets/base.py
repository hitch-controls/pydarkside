from pydarkside.qt import qtw
from pydarkside.conf import get_settings


class ThemedWidget(qtw.QWidget):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

