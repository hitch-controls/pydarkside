import sys


if sys.platform == 'win32':
    from pydarkside.platform.win32.main_window import DSWin32MainWindow as DSMainWindow
else:
    from .core import DSMainWindow
