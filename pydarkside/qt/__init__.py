from . import uic, QtCore, QtWidgets, QtGui, QtOpenGL, QtWebEngineWidgets
from PySide6 import QtSvgWidgets, QtSvg  # noqa

# The wrapper modules included above are only needed so we can create these shortcuts
qtc = QtCore
qtw = QtWidgets
qtwe = QtWebEngineWidgets
qtg = QtGui
qtgl = QtOpenGL
